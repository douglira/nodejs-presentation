module.exports = (sequelize, DataTypes) => {
  const Permissao = sequelize.define('Permissao', {
    nome: DataTypes.STRING
  }, {
    tableName: 'permissoes'
  })

  Permissao.associate = (models) => {
    Permissao.belongsToMany(models.Usuario, { as: 'usuarios', through: 'UsuariosPermissoes', foreignKey: 'permissao_id', otherKey: 'usuario_id' })
  }

  return Permissao
}
