const bcrypt = require('bcryptjs')

module.exports = (sequelize, DataTypes) => {
  const Usuario = sequelize.define('Usuario', {
    nome: DataTypes.STRING,
    email: DataTypes.STRING,
    senha: DataTypes.VIRTUAL,
    hash_senha: DataTypes.STRING,
    admin: DataTypes.BOOLEAN
  }, {
    tableName: 'usuarios',
    defaultScope: {
      attributes: {
        exclude: ['hash_senha']
      }
    },
    hooks: {
      beforeSave: async usuario => {
        if (usuario.senha) {
          usuario.hash_senha = await bcrypt.hash(usuario.senha, 10)
        }
      }
    }
  })

  Usuario.associate = (models) => {
    Usuario.belongsToMany(models.Permissao, { as: 'permissoes', through: models.UsuarioPermissao, foreignKey: 'usuario_id', otherKey: 'permissao_id' })
  }

  return Usuario
}
