module.exports = (sequelize, DataTypes) => {
  const UsuarioPermissao = sequelize.define('UsuarioPermissao', {}, {
    tableName: 'usuarios_permissoes',
    timestamps: false
  })

  return UsuarioPermissao
}
