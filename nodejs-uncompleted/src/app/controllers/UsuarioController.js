const { Usuario, Permissao, sequelize } = require('../models')

class UsuarioController {
  async save (req, res) {
    const { usuario, permissoes } = req.body

    const permissoesEncontradas = await Permissao.findAll({ where: { id: permissoes } })

    let usuarioBuilder = new Usuario(usuario)
    await sequelize.transaction(async (transaction) => {
      usuarioBuilder = await usuarioBuilder.save({ transaction })
      await usuarioBuilder.setPermissoes(permissoesEncontradas, { transaction })
    })

    await usuarioBuilder.reload({
      include: [{ model: Permissao, as: 'permissoes' }]
    })

    res.json({
      ...usuarioBuilder.toJSON(),
      senha: undefined,
      hash_senha: undefined
    })
  }

  async findAll (req, res) {
    const usuarios = await Usuario.findAll({ limit: 10, include: [{ model: Permissao, as: 'permissoes' }] })

    res.json(usuarios)
  }
}

module.exports = new UsuarioController()
