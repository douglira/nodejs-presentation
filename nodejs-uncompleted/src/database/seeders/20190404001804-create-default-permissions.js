'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('permissoes', [
      { nome: 'ADMINISTRADOR_GLOBAL' },
      { nome: 'ADMINISTRADOR' },
      { nome: 'USUARIO' }
    ])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('permissoes', {
      name: ['ADMINISTRADOR_GLOBAL', 'ADMINISTRADOR', 'USUARIO']
    })
  }
}
