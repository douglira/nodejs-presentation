const express = require('express')
const requireDir = require('require-dir')

const {
  UsuarioController
} = requireDir('./app/controllers')

const routes = express.Router()

routes.post('/usuarios', UsuarioController.save)
routes.get('/usuarios', UsuarioController.findAll)

module.exports = express.Router().use('/api', routes)
