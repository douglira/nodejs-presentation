const express = require('express')
const app = express()

app.use(express.json())
app.use(express.urlencoded())

app.get('/teste', (req, res, next) => {
  console.log('primeiro')
  next()
})
app.get('/teste', (req, res) => {
  console.log('segundo')
  res.json({ helloWorld: 'yeah' })
})

app.post('/teste', (req, res) => {
  console.log(req.body) // {}
  res.json({ helloWorld: 'yeah' })
})

app.listen(8080)
